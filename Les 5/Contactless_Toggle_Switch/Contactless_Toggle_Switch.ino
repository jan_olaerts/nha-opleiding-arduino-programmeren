  const int SENSOR_PIN = 2;
  const int LED_PIN = 13;

  int state = 1;
  int count = 0;
  int ledState = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  pinMode(SENSOR_PIN, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  state = digitalRead(SENSOR_PIN);

  if(state == LOW) {
    ledState = !ledState;
    delay(50);
  }

  digitalWrite(LED_PIN, ledState);
}
