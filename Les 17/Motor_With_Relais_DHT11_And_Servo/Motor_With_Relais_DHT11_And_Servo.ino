#include <DHT.h>
#include <Servo.h>

DHT dht;
Servo servo;
const int DHT11Pin = A1;
const int relaisPin = 7;
const int servoPin = 9;
const int HALF_CLOCKWISE_TURN = 0;
const int HALF_COUNTER_CLOCKWISE_TURN = 180;

void setup() {
  Serial.begin(9600);
  dht.setup(DHT11Pin); // Set DHT11 sensor to pin A1
  servo.attach(servoPin); // Set servo to pin D9
  pinMode(relaisPin, OUTPUT); // Set motor to pin D7
}

void loop() {
  delay(1500); // Give DHT time to sample data
  float humidity = dht.getHumidity(); // Getting the humidity in %
  Serial.println(humidity);
  if(humidity >= 65) sendSignalToVentilatorAndServo(HIGH, HALF_COUNTER_CLOCKWISE_TURN); // Turn on motor and make servo turn 180 degrees counter clockwise
  else sendSignalToVentilatorAndServo(LOW, HALF_CLOCKWISE_TURN); // Turn off motor and make servo turn 180 degrees clockwise
}

void sendSignalToVentilatorAndServo(int ventilatorState, int servoDegrees) {
  digitalWrite(relaisPin, ventilatorState); // Set the motor to the passed state
  servo.write(servoDegrees); // Set the servo to the passed degrees
  delay(15); // Wait until servo position is reached
}