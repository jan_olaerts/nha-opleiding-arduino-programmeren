#include <Servo.h>

Servo servo;
int potPin = 0; // Potentiometer at analog input A0
int val;        // Variable for the input of A0

void setup() {
  servo.attach(9); // Servomotor at pin D9
}

void loop() {
  val = analogRead(potPin); // Read value potentiometer 0...1023
  val = map(val, 0, 1023, 0, 180); // Map value potentiometer to scope 0...180
  servo.write(val); // Give wanted degrees to servo
  delay(15); // Wait until position is reached
}
