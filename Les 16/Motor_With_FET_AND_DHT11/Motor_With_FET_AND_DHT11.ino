#include "DHT.h";
DHT dht;

const int FET_PIN = 11;
const int DHT11_Pin = A5;
const int FULL_SPEED = 255;
const int HALF_SPEED = 128;
const int NO_SPEED = 0;

int outputValue = 0;

void setup() {
  Serial.begin(9600);
  dht.setup(DHT11_Pin);
}

void loop() {
  delay(1500); // Give DHT11 time to sample data
  float temperature = dht.getTemperature();
  analogWrite(FET_PIN, getOutputValue(temperature));
  Serial.print("temperature = ");
  Serial.println(temperature, 2);
  Serial.print("\t output = ");
  Serial.println(outputValue);
}

int getOutputValue(float temperature) { // Method to get outputValue according to temperature
  if(temperature <= 21) outputValue = NO_SPEED;
  else if(temperature > 21 && temperature <= 25) 
    outputValue = startMotorAtHalfSpeed();
  else outputValue = FULL_SPEED;
  return outputValue;
}

int startMotorAtHalfSpeed() { // Method to give booster to motor when starting to run on half speed
  if(outputValue == NO_SPEED) { // Only give booster if motor needs to be started from 0
    analogWrite(FET_PIN, FULL_SPEED);
    delay(500); // Booster only lasts for 500 milliseconds
  }
  return HALF_SPEED;
}