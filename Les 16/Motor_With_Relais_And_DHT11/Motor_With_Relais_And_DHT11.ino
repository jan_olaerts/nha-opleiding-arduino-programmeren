#include "DHT.h";
DHT dht;

const int DHT11Pin = A1;
const int relaisPin = 7;

void setup() {
  Serial.begin(9600);
  dht.setup(DHT11Pin);
  pinMode(relaisPin, OUTPUT);
}

void loop() {
  delay(1500);
  float temperature = dht.getTemperature();
  Serial.println(temperature);
  if(temperature >= 25.0) digitalWrite(relaisPin, HIGH);
  else digitalWrite(relaisPin, LOW);
}
