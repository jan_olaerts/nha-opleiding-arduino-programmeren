void setup() {
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

int count = 0;

void loop() {

  while (count < 5) {
    blink(10);
    count++;
  }

  while (count < 10) {
    blink(11);
    count++;
  }

  count = 0;
}

void blink(int ledNumber) {
  digitalWrite(ledNumber, HIGH);
  delay(200);
  digitalWrite(ledNumber, LOW);
  delay(200);
}
