void setup() {
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
}

void loop() {
  showSOSSignal();
}

void showSOSSignal() {

  doThreeShortPulses();
  doTwoLongPulses();
  doThreeShortPulses();
  delay(2000);
}

void doThreeShortPulses() {
    for(int i = 0; i < 3; i++) {
    digitalWrite(1, HIGH);
    digitalWrite(2, HIGH);
    delay(200);
    digitalWrite(1, LOW);
    digitalWrite(2, LOW);
    delay(200);
  }
}

void doTwoLongPulses() {
  for(int i = 0; i < 2; i++) {
    delay(200);
    digitalWrite(1, HIGH);
    digitalWrite(2, HIGH);
    delay(400);
    digitalWrite(1, LOW);
    digitalWrite(2, LOW);
    delay(200);
  }
}