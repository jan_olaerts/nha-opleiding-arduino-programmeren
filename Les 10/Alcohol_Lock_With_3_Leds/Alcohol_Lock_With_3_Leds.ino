const int analogInPin = A0; // pin number analog input MQ-3 sensor
const int ledPinRed = 2;
const int ledPinYellow = 3;
const int ledPinGreen = 4;

int sensorValue = 0; // value read from MQ-3 sensor
int outputValue = 0;
// value after mapping

void setup() { 
  pinMode(ledPinGreen, OUTPUT);
  pinMode(ledPinYellow, OUTPUT);
  pinMode(ledPinRed, OUTPUT);
  Serial.begin(9600); // initialize speed of serial communication to 9600
}

void loop() {
  sensorValue = analogRead(analogInPin); // read the analog value from the sensor
  outputValue = map(sensorValue, 199, 1023, 50, 10000); // map the value from A0 to a value between 50 - 1000 μg/L
  Serial.print("Digital sensor value = ");
  Serial.print(sensorValue);
  Serial.print("\t Alcohol value = ");
  Serial.print(outputValue);
  Serial.println("\t ug/L");

  // check if value is beneath limit for a beginning driver
  if(outputValue <= 88) {
    digitalWrite(ledPinGreen, HIGH);
  } else {
    digitalWrite(ledPinGreen, LOW);
  }

  // check if value is beneath limit for a medior driver
    if((outputValue > 88) && (outputValue < 220)) {
    digitalWrite(ledPinYellow, HIGH);
  } else {
    digitalWrite(ledPinYellow, LOW);
  }

  // check if value is beneath limit for an experienced driver
    if(outputValue >= 220) {
    digitalWrite(ledPinRed, HIGH);
  } else {
    digitalWrite(ledPinRed, LOW);
  }

  // repeat measurement every second
  delay(1000);
}
