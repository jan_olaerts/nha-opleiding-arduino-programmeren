const int AOpin = 0;
const int DOpin = 8;
const int ledPin = 13;

int threshold;
int value;

void setup() {
  Serial.begin(9600);
  pinMode(DOpin, INPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  value = analogRead(AOpin);
  threshold = digitalRead(DOpin);
  Serial.print("Alcohol value: ");
  Serial.println(value);
  Serial.print("Threshold: ");
  Serial.println(threshold);
  delay(1000);

  if(threshold == HIGH) {
    digitalWrite(ledPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
  }
}
