const int analogInPin = A0;
const int analogOutPin_1 = 9;
const int analogOutPin_2 = 11;

int sensorValue = 0;
int outputValue = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  sensorValue = analogRead(analogInPin);
  outputValue = map(sensorValue, 0, 1023, 0, 255); // 255 = 100%

  analogWrite(analogOutPin_1, outputValue);
  analogWrite(analogOutPin_2, 255 - outputValue);

  Serial.print("sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t outputValue 1 = ");
  Serial.println(outputValue);
  Serial.print("\t outputValue 2 = ");
  Serial.println(255 - outputValue);

  delay(2);
}
