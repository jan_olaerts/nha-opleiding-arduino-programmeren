#include <DHT.h>
#include <Adafruit_LiquidCrystal.h>

DHT dht;
Adafruit_LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  dht.setup(6);
  lcd.begin(16, 2); // display can contain 16 characters and 2 alineas
}

void loop() {
  delay(dht.getMinimumSamplingPeriod());
  float temperature = dht.getTemperature();
  float humidity = dht.getHumidity();
  lcd.setCursor(0, 0);
  lcd.print("Temp: ");
  lcd.setCursor(6, 0);
  lcd.println(temperature, 0);
  lcd.setCursor(8, 0);
  lcd.print(" oC");
  lcd.setCursor(0, 1);
  lcd.print("Hum: ");
  lcd.setCursor(6, 1);
  lcd.println(humidity, 0);
  lcd.setCursor(8, 1);
  lcd.print("  %");
}