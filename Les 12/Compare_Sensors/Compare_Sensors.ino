#include <DHT.h>
#include <Adafruit_LiquidCrystal.h>

DHT dht;
Adafruit_LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int LM35Pin = A5;
int LM35Value = 0;

void setup() {
  Serial.begin(9600);
  dht.setup(6);
  lcd.begin(16, 2); // display can contain 16 characters and 2 alineas
}

void loop() {
  delay(1000);
  displayTemperature("Temp DHT: ", 0, dht.getTemperature());
  displayTemperature("Temp LM: ", 1, (float) map(analogRead(LM35Pin), 0, 1023, 0, 255));
}

void displayTemperature(String text, int rule, float temperature) {
  lcd.setCursor(0, rule);
  lcd.print(text);
  lcd.setCursor(9, rule);
  lcd.println(temperature, 1);
  lcd.setCursor(13, rule);
  lcd.print(" oC");
}