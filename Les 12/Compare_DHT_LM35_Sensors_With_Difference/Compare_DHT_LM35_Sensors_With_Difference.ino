#include <DHT.h>
#include <Adafruit_LiquidCrystal.h>

DHT dht;
Adafruit_LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int LM35Pin = A5;
int LM35Value = 0;

void setup() {
  Serial.begin(9600);
  dht.setup(6);
  lcd.begin(16, 2); // display can contain 16 characters and 2 alineas
}

void loop() {
  delay(1000);
  float temperature1 = dht.getTemperature();
  float temperature2 = (float) map(analogRead(LM35Pin), 0, 1023, 0, 255);
  displayTemperatures(temperature1, temperature2);
  displayTempDifference(temperature1, temperature2);
}

void displayTemperatures(float temp1, float temp2) {
  lcd.setCursor(0, 0);
  lcd.print("T1:");
  lcd.setCursor(3, 0);
  lcd.println(temp1, 1);
  lcd.setCursor(8, 0);
  lcd.print("T2:");
  lcd.setCursor(11, 0);
  lcd.println(temp2, 1);
}

void displayTempDifference(float temp1, float temp2) {
  float difference;
  if(temp1 > temp2) difference = temp1 - temp2;
  else difference = temp2 - temp1;

  lcd.setCursor(0, 1);
  lcd.print("Diff:");
  lcd.setCursor(6, 1);
  lcd.println(difference, 1);
  lcd.setCursor(9, 1);
  lcd.print(" oC");
}