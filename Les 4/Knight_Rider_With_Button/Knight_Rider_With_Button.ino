const int BUTTON_PIN = 10;
int buttonState = 1;
boolean buttonPressed = false;

void setup() {
  for(int i = 1; i < 9; i++)
    pinMode(i, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);
}

void loop() {

  buttonState = digitalRead(BUTTON_PIN);
  if(buttonState == 1) buttonPressed = true;

  if(buttonState == LOW && buttonPressed) {
    int count = 0;
    while(count < 5) {
      lightOneToEight();
      lightEightToOne();
      count++;
    }
  }
}

void lightOneToEight() {

  for(int i = 1; i <= 4; i++) {
    turnLedOn(i, 50);
  }

  for(int i = 1; i <= 8; i++) {
    turnLedOff(i, 50);
    if(i <= 4) turnLedOn(i+4, 50);
  }
}

void lightEightToOne() {

  for(int i = 8; i >= 5; i--) {
    turnLedOn(i, 50);
  }

  for(int i = 8; i >= 1; i--) {
    turnLedOff(i, 50);
    if(i >= 5) turnLedOn(i-4, 50);
  }
}

void turnLedOn(int ledNum, int ms) {
  digitalWrite(ledNum, HIGH);
  delay(ms);
}

void turnLedOff(int ledNum, int ms) {
  digitalWrite(ledNum, LOW);
  delay(ms);
}