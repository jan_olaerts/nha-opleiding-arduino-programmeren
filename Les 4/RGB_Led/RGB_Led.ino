const int RED_PIN = 2;
const int GREEN_PIN = 3;
const int BLUE_PIN = 4;

void setup() {
  // put your setup code here, to run once:
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  turnColorsOn(false, false, false);
  turnColorsOn(false, false, true);
  turnColorsOn(false, true, false);
  turnColorsOn(false, true, true);
  turnColorsOn(true, false, false);
  turnColorsOn(true, false, true);
  turnColorsOn(true, true, false);
  turnColorsOn(true, true, true);
}

void turnColorsOn(boolean red, boolean green, boolean blue) {
  if(red) digitalWrite(RED_PIN, HIGH);
  if(green) digitalWrite(GREEN_PIN, HIGH);
  if(blue) digitalWrite(BLUE_PIN, HIGH);
  delay(1000);
  turnAllColorsOff();
}

void turnAllColorsOff() {
  digitalWrite(RED_PIN, LOW);
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(BLUE_PIN, LOW);
}