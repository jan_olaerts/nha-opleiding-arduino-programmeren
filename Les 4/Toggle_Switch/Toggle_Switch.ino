const int BUTTON_PIN = 2;
const int LED_PIN =  13;

int buttonState = 0;
int count = 0;
boolean buttonPressed = false;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
}

void loop() {
  buttonState = digitalRead(BUTTON_PIN);

  if(buttonState == HIGH) {
    if(count == 0) buttonPressed = true;
    count++;
    Serial.print(count);
    delay(50);
  }

  if(buttonState == LOW && buttonPressed){
    count++;
    buttonPressed = false;
    Serial.print(count);
    delay(50);
  }

  count % 2 == 0 ? digitalWrite(13, LOW) : digitalWrite(13, HIGH);
}