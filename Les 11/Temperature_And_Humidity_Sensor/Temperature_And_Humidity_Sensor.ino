#include "DHT.h";
DHT dht;

const int temperatureLed = 11;
const int humidityLed = 12;

void setup() {
  Serial.begin(9600);                       // Starting of serial communication
  Serial.println();
  Serial.println("Status\tRelative air moisture (%)\tTemperature in degrees C\t");
  dht.setup(2);                             // Configuration of DHT sensor and connect it to pin D2

  pinMode(temperatureLed, OUTPUT);
  pinMode(humidityLed, OUTPUT);
}

void loop() {
  delay(dht.getMinimumSamplingPeriod());    // Pause to let DHT gather new data
  float humidity = dht.getHumidity();       // Get humidity
  float temperature = dht.getTemperature(); // Get temperature
  Serial.print(dht.getStatusString());        // Print status of DHT sensor to serial monitor
  Serial.print("\t");
  Serial.print(humidity, 2);                // Print relative humidity at 2 decimals
  Serial.print("\t\t\t\t");
  Serial.println(temperature, 2);           // Print temperature at 2 decimals
  checkForTemperature(temperature, 21.00);
  checkForHumidity(humidity, 60.00);
}

void checkForTemperature(float temperature, float treshold) {
  int writeValue = temperature < treshold ? HIGH : LOW;
  digitalWrite(temperatureLed, writeValue);
}

void checkForHumidity(float humidity, float treshold) {
  int writeValue = humidity > treshold ? HIGH : LOW;
  digitalWrite(humidityLed, writeValue);
}