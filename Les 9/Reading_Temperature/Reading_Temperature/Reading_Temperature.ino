int LM35pin = A0;
int LM35Value = 0;
int intCelsius = 0;
int voltage = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  LM35Value = analogRead(LM35pin);
  intCelsius = map(LM35Value, 0, 205, 0, 100);
  voltage = map(LM35Value, 0, 1000, 0, 5000);
  Serial.print("Degrees Celsius: ");
  Serial.println(intCelsius);
  Serial.print("Voltage in mV: ");
  Serial.println(voltage);
  Serial.println("\t");
  delay(1000);
}