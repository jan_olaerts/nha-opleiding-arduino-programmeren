const int sensorPin = A5;
int sensorValue = 0;

void setup() {
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {

  sensorValue = analogRead(sensorPin);

  if(sensorValue > 1000) {
      daySimulation();
  } else {
    nightSimulation();
  }
}

void daySimulation() {
  carTrafficLightOnRedDuringDay();
}

void nightSimulation() {
  digitalWrite(4, HIGH);
  digitalWrite(3, HIGH);
  if(analogRead(sensorPin) > 1000) {
    digitalWrite(3, LOW);
    carTrafficLightOnRedDuringNight();
  } 
}

void carTrafficLightOnRedDuringDay() {
  digitalWrite(3, LOW);
  orangeCarLight();
  digitalWrite(1, HIGH);
  bicycleTrafficLightOnGreen();
  delay(93000);
  orangeCarLight();
  digitalWrite(1, LOW);
  digitalWrite(3, HIGH);
  delay(116500);
}

void carTrafficLightOnRedDuringNight() {
  digitalWrite(1, HIGH);
  bicycleTrafficLightOnGreen();
  delay(10000);
  digitalWrite(1, LOW);
}

void bicycleTrafficLightOnGreen() {
  orangeBicycleLight();
  digitalWrite(4, LOW);
  digitalWrite(6, HIGH);
  delay(20000);
  orangeBicycleLight();
  digitalWrite(6, LOW);
  digitalWrite(4, HIGH);
}

void orangeCarLight() {
  digitalWrite(2, HIGH);
  delay(3500);
  digitalWrite(2, LOW);
}

void orangeBicycleLight() {
  digitalWrite(5, HIGH);
  delay(3500);
  digitalWrite(5, LOW);
}