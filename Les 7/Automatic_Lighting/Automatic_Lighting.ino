const int buttonPin = 10;
const int ledPin = 13;
const int sensorPin = A0;

int buttonState = 0;
int sensorValue = 0;

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  buttonState = digitalRead(buttonPin);
  sensorValue = analogRead(sensorPin);

  if(buttonState == HIGH) {
    digitalWrite(ledPin, HIGH);
  }

  if(sensorValue > 1000) {
    digitalWrite(ledPin, LOW);
  }
}
