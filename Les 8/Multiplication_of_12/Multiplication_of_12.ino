void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Multiplication of 12");

  for (int i = 1; i < 11; i++) {
    Serial.print(i);
    Serial.print(" x 12 = ");
    Serial.println(i * 12);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
}
