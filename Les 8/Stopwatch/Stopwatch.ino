void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print(" Number of seconds after start:\tNumber of seconds until 1000: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  
  // print number of seconds after start
  Serial.print(millis() / 1000);
  Serial.print("\t\t\t\t");
  Serial.println(1000-(millis()/1000));
  // wait for 1 second
  delay(1000);
}
