const int sensorPin = A5;
const int ledPin = 13;
int sensorValue = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);

  // start serialization
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue = analogRead(sensorPin);

  // printing sensorValue
  Serial.print("sensorValue potentiometer:");
  Serial.print("\t\t\t\t");
  Serial.println(sensorValue);

  // blinking led
  digitalWrite(ledPin, HIGH);
  delay(sensorValue);
  digitalWrite(ledPin, LOW);
  delay(sensorValue);
}
