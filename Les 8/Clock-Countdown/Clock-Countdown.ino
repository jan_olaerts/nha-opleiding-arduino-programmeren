void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("From 1 minute to 0");
}

void loop() {
  // put your main code here, to run repeatedly:
  int seconds;
  seconds = (60000-millis())/1000;

  if(seconds == 0) {
    Serial.print("0 reached");
    delay(1000);
    exit(0);
  }

  Serial.print(seconds);
  Serial.print("\t\t\t\t");
  Serial.print(" seconds to go");
  Serial.print("\n");
  delay(1000);
}
