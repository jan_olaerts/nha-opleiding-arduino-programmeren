int rpm = 0;
volatile int rpmCount = 0;
unsigned long lastMillis = 0;

void setup() {
  Serial.begin(9600);

  // start interruption, meaning: when D2 registers 0V, invoke rpm_fan()
  attachInterrupt(0, rpm_fan, FALLING);
}

void loop() {
  // if there is new data after 1s
  if (millis() - lastMillis == 1000) {
    
    // disable interruption temporarily
    detachInterrupt(0);

    // calculate rpm on 1 puls per rotation (2 magnets)
    rpm = rpmCount * 30;

    Serial.print("RPM =\t");
    Serial.print(rpm);
    Serial.print("\t Hz =\t");
    Serial.println(rpmCount);

    // set counter to zero and start counting again
    rpmCount = 0;

    // count the last milliseconds
    lastMillis = millis();

    // enable interruption since calculation is ready
    attachInterrupt(0, rpm_fan, FALLING);
  }
}

void rpm_fan() {
  rpmCount++;
}