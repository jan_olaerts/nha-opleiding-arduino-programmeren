#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#define BACKLIGHT_PIN 13

LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

void setup() {
  // put backlight
  pinMode(BACKLIGHT_PIN, OUTPUT);
  digitalWrite(BACKLIGHT_PIN, HIGH);

  // define 16 characters and 2 alineas
  lcd.begin(16, 2);
}

void loop() {
  lcd.setCursor(0, 0); // set cursus at position 0, alinea 0
  lcd.print("Hallo, België");
  lcd.setCursor(0, 1);
  lcd.print("Alles goed?");
  delay(2000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Hallo, België");
  lcd.setCursor(0, 1);
  lcd.print("Jazeker");
  delay(2000);
  lcd.clear();
}