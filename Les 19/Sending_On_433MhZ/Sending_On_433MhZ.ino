#include <VirtualWire.h>

const int led_pin = 11;
const int transmit_pin = 12;
int LM35Pin = A0;
int LM35Value = 0;

void setup() {
  vw_set_tx_pin(transmit_pin);
  vw_setup(2000); // communication speed 2000 bits per second
  pinMode(led_pin, OUTPUT);
}

void loop() {
  LM35Value = analogRead(LM35Pin);
  char msg[1] = {LM35Value/2}; // make message 1
  digitalWrite(led_pin, HIGH); // led lights up, sending started
  vw_send((uint8_t *)msg, 1); // send message 1 as serial stream of 8 bits
  vw_wait_tx(); // wait until all data is sent
  digitalWrite(led_pin, LOW); // led off, sending is done
  delay(1000);
}
