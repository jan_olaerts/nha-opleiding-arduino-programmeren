#include <SPI.h>
#include <SD.h>

const int chipSelect = 4;
const int analogInPin0 = A0; // LDR
const int analogInPin1 = A1; // potentiometer 1
const int analogInPin2 = A2; // potentiometer 2

int sensorValue1 = 0; // variable for value LDR
int sensorValue2 = 1; // variable for value potentiometer 1
int sensorValue3 = 2; // variable for value potentiometer 2

void setup() {
  Serial.begin(9600);
  while(!Serial) {;} // waiting for serial communication
  pinMode(4, OUTPUT); // resetting shield functions
  digitalWrite(4, HIGH);
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH); // shield type W5100

  Serial.print("Initialising SD card");
  if(!SD.begin(chipSelect)) { // check whether SD card is in reader
    Serial.println("Card not in reader");
    return;
  }

  Serial.println("Card ready");
}

void loop() {
  sensorValue1 = analogRead(analogInPin0);
  sensorValue2 = analogRead(analogInPin1);
  sensorValue3 = analogRead(analogInPin2);

  // making string, read 3 times sensor value and attach them to the string
  String dataString = "";
  
  for(int analogPin = 0; analogPin < 3; analogPin++) {
    int sensor = analogRead(analogPin);
    dataString += String(sensor);
  
    if(analogPin < 2) // two commas per alinea 
      dataString += ","; // place comma behind sensor value
  }

  // opening file
  File dataFile = SD.open("DATALOG.txt", FILE_WRITE);

  if(dataFile) { // if file is accessible
    dataFile.println(dataString); // write data
    dataFile.close();
    Serial.println("");
    Serial.println("New");
    Serial.print("LDR value = ");
    Serial.println(sensorValue1);
    Serial.print("Potentiometer 1 value = ");
    Serial.println(sensorValue2);
    Serial.print("Potentiometer 2 value = ");
    Serial.println(sensorValue3);
    Serial.print("Saved to file DATALOG.TXT: ");
    Serial.print(dataString);
    Serial.println("");
  } else {
    // error message if file is not accessible
    Serial.println("Error while opening DATALOG.TXT")
  }

  delay(1000);
}
