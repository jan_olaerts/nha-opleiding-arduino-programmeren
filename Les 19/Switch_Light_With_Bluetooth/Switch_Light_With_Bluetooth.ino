#include <SoftwareSerial.h>

// define receiving and sending on pins D0 and D2
const int RX_BT = 0;
const int TX_BT = 2;
SoftwareSerial btSerial(TX_BT, RX_BT);

const int LED_PIN = 7;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);
  Serial.println("Bluetooth connection");
  btSerial.begin(9600);
}

void loop() {
  if (btSerial.available()) {

      int commandSize = (int) btSerial.read();
      char command[commandSize];
      int commandPos = 0;

      while (commandPos < commandSize) {

        if (btSerial.available()) {

          command[commandPos] = (char) btSerial.read();
          commandPos++;
        }
      }

    command[commandPos] = 0;
    processCommand(command);  
  }
}

void processCommand(char* command) {
  Serial.println(command);
  String strCommand = (String) command;

  if (strCommand == "on") {
    Serial.println(1);
    digitalWrite(LED_PIN, HIGH);
  } else if (strCommand == "off") {
    Serial.println(0);
    digitalWrite(LED_PIN, LOW);
  }
}
