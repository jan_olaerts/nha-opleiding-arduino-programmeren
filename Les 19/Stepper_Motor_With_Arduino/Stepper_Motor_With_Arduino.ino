int i1 = 8;
int i2 = 9;
int i3 = 10;
int i4 = 11;

int step = 0;
int sensorPin = A0;

const int buttonPin = 2;

int sensorValue = 0; // potentiometer
int buttonState = 0; // switch

void setup() {
  pinMode(i1, OUTPUT);
  pinMode(i2, OUTPUT);
  pinMode(i3, OUTPUT);
  pinMode(i4, OUTPUT);
}

void loop() {
  buttonState = digitalRead(buttonPin);
  sensorValue = analogRead(sensorPin);

  switch(step) {
    case 0:
      digitalWrite(i1, LOW);
      digitalWrite(i2, LOW);
      digitalWrite(i3, LOW);
      digitalWrite(i4, HIGH);
      break;
     case 1:
      digitalWrite(i1, LOW); 
      digitalWrite(i2, LOW); 
      digitalWrite(i3, HIGH); 
      digitalWrite(i4, HIGH);
      break;
    case 2:
      digitalWrite(i1, LOW); 
      digitalWrite(i2, LOW); 
      digitalWrite(i3, HIGH); 
      digitalWrite(i4, LOW);
      break; 
    case 3:
      digitalWrite(i1, LOW); 
      digitalWrite(i2, HIGH); 
      digitalWrite(i3, HIGH); 
      digitalWrite(i4, LOW);
      break; 
    case 4:
      digitalWrite(i1, LOW); 
      digitalWrite(i2, HIGH); 
      digitalWrite(i3, LOW); 
      digitalWrite(i4, LOW);
      break; 
    case 5:
      digitalWrite(i1, HIGH); 
      digitalWrite(i2, HIGH); 
      digitalWrite(i3, LOW); 
      digitalWrite(i4, LOW);
      break; 
    case 6:
      digitalWrite(i1, HIGH); 
      digitalWrite(i2, LOW); 
      digitalWrite(i3, LOW); 
      digitalWrite(i4, LOW);
      break; 
    case 7:
      digitalWrite(i1, HIGH); 
      digitalWrite(i2, LOW); 
      digitalWrite(i3, LOW); 
      digitalWrite(i4, HIGH);
      break; 
    default:
      digitalWrite(i1, LOW); 
      digitalWrite(i2, LOW); 
      digitalWrite(i3, LOW); 
      digitalWrite(i4, LOW);
      break; 
  }

  if(buttonState == HIGH) {
    step++; // button decides direction
  } else {
    step--;
  }

  if(step > 7) step = 0;
  if(step < 0) step = 7;

  delay(sensorValue); // potentiometer decides speed
}