#include <VirtualWire.h>
#include <DHT.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>

DHT dht;
Adafruit_BMP085 bmpSensor;
const int led_pin = 11;
const int transmit_pin = 12;
const int LM35Pin = A0;
const int LDRPin = A2;
const int DHT11Pin = 7;

void setup() {
  vw_set_tx_pin(transmit_pin);
  vw_setup(2000); // communication speed 2000 bits per second
  pinMode(led_pin, OUTPUT);
  dht.setup(DHT11Pin);
  bmpSensor.begin();
}

void loop() {
  delay(1000); // pause to let DHT gather new data
  int LM35Value = analogRead(LM35Pin);
  int lumen = ldrValueToLumen(analogRead(LDRPin));
  float humidity = dht.getHumidity();
  float pressureInBar = bmpSensor.readPressure()/101.325;

  char msg[4] = {LM35Value/2, humidity, lumen, pressureInBar}; // make message

  digitalWrite(led_pin, HIGH); // led lights up, sending started
  vw_send((uint8_t *)msg, 4); // send message 1 as serial stream of 8 bits

  vw_wait_tx(); // wait until all data is sent
  digitalWrite(led_pin, LOW); // led off, sending is done
}

int ldrValueToLumen(int ldrValue) {
  float vOut = float (ldrValue) * (5 / float(1023)); // conversion analog to voltage (5 is voltage)
  float rldr = (10000 * (5 - vOut)) / vOut; // conversion voltage to resistance (5 is voltage, 10000 is resistance)
  return 500 / (rldr / 1000); // conversion resistance to lumen
}