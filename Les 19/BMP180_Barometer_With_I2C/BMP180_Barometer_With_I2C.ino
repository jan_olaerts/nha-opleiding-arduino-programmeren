#include "Wire.h"
#include "Adafruit_BMP085.h"

Adafruit_BMP085 bmpSensor;

void setup() {
  Serial.begin(9600);
  bmpSensor.begin();
}

void loop() {
  Serial.print("Temp = ");
  Serial.print(bmpSensor.readTemperature());
  Serial.println(" °C");

  Serial.print("Air pressure = ");
  Serial.print(bmpSensor.readPressure());
  Serial.println(" Pa of ");

  float pressureInBar = bmpSensor.readPressure()/101.325;
  Serial.print(pressureInBar);
  Serial.println(" mbar");

  Serial.println();
  delay(1000);
}
