#include <VirtualWire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

const int led_pin = 13;
const int receive_pin = 11;

void setup() {
  lcd.init(); // initalize lcd display
  lcd.backlight();
  lcd.begin(16, 2); // start display with 16 characters and 2 rules
  delay(1000);
  vw_set_rx_pin(receive_pin); // set receiver on pin 11
  vw_setup(2000); // communication speed 2000 bits per second
  vw_rx_start(); // start receiving
  pinMode(led_pin, OUTPUT);
}

void loop() {
  // set universal receive buffer
  uint8_t buf[VW_MAX_MESSAGE_LEN];
  // determine max length of receive buffer
  uint8_t bufLen = VW_MAX_MESSAGE_LEN;

  // read buffer content when 8 bits received
  if(vw_get_message(buf, &bufLen)) {
    int i;
    digitalWrite(led_pin, HIGH);
    lcd.setCursor(0, 0);
    lcd.print("Temp = ");
    lcd.setCursor(7, 0);
    lcd.print(buf[i], DEC); // show content buffer in decimal
    lcd.setCursor(0, 1);
    lcd.print("Degrees Celsius ");
    for(i = 0; i < bufLen; i++) // next message
    digitalWrite(led_pin, LOW);
  }
}
