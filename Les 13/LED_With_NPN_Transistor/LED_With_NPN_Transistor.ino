int FET_PIN = 11;
int TRANSISTOR_PIN = 12;

void setup() {
  pinMode(FET_PIN, OUTPUT);
  pinMode(TRANSISTOR_PIN, OUTPUT);
}

void loop() {
  digitalWrite(FET_PIN, HIGH);
  digitalWrite(TRANSISTOR_PIN, LOW);
  delay(5000);
  digitalWrite(FET_PIN, LOW);
  digitalWrite(TRANSISTOR_PIN, HIGH);
  delay(5000);
}
